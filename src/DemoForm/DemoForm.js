import React, { Component } from "react";

export default class extends Component {
  state = {
    username: "Alice",
    password: "123456",
  };

  handleLogin = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    this.setState({ [name]: value });
  };
  render() {
    return (
      <div className="container py-5">
        <form action="">
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              name="username"
              placeholder="Username"
              value={this.state.username}
              onChange={(e) => {
                this.handleLogin(e);
              }}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              name="password"
              placeholder="Password"
              value={this.state.password}
              onChange={(e) => {
                this.handleLogin(e);
              }}
            />
          </div>

          <button className="btn btn-warning">Login</button>
        </form>
      </div>
    );
  }
}

// let sv = {
//   name: "Alice",
//   gender: "Femail",
// };
// let targetName = "gender";
// let value = sv[targetName];
// console.log("value", value);
