// tổ chức và xử lý state

// tổ chức
let initialState = {
  giaTri: 10,
};

// xử lý
export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case "TANG_SO_LUONG": {
      state.giaTri++;
      return { ...state };
    }
    case "GIAM_SO_LUONG": {
      state.giaTri--;
      return { ...state };
    }

    default:
      return state;
  }
};
