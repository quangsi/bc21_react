import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export let rootReducer = combineReducers({
  number: numberReducer,
});

//  rootReducer quản lý các sate nhỏ dưới dạng object
