import React, { Component } from "react";
import { connect } from "react-redux";

class DemoMiniRedux extends Component {
  render() {
    return (
      <div>
        {this.props.giaTri}
        <button
          onClick={() => {
            this.props.tangSoLuong();
          }}
        >
          Increase
        </button>

        <button onClick={this.props.giaSoLuong}>Decrease</button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    giaTri: state.number.giaTri,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: () => {
      let action = {
        type: "TANG_SO_LUONG",
      };

      dispatch(action);
    },
    giaSoLuong: () => {
      dispatch({
        type: "GIAM_SO_LUONG",
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoMiniRedux);
