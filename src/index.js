import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { rootReducerXucSac } from "./BaiTapGameXucSacRedux/redux/reducers/rootReducer";
import { monAnReducer } from "./DemoAxios/redux_monAn/reducer/monAn.reducer";
import { rootReducerXucSacHook } from "./BaiTapGameXucSacReduxHook/redux/reducers/rootReducer";
// import { rootReducer } from "./BaiTapGioHangRedux/redux_baiTapGioHang/reducer/rootReducer";
// import { rootReducer } from "./DemoMiniRedux/redux_demomini/reducer/rootReducer";

const root = ReactDOM.createRoot(document.getElementById("root"));

let storeRedux = createStore(
  rootReducerXucSacHook,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
root.render(
  <Provider store={storeRedux}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
