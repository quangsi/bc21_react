import React, { Component } from "react";
import { connect } from "react-redux";
import { playGameAction } from "./redux/actions/xucSacAction";
class KetQua extends Component {
  render() {
    let { luaChon, soBanThang, soLuotChoi } = this.props;
    return (
      <div>
        {/* {luaChon && <h4> Kết quả </h4>} */}
        {luaChon && <h3 className=" text-success">Lựa chọn :{luaChon}</h3>}
        {/* {luaChon || <h1>Bạn chưa chọn</h1>} */}
        <h3 className=" text-danger">Số bàn thắng :{soBanThang}</h3>
        <h3 className=" text-dark">Số lượt chơi :{soLuotChoi}</h3>
        <button
          className="btn btn-warning "
          onClick={() => {
            this.props.handlePlay();
          }}
        >
          <span className="display-4">Play Game</span>
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    luaChon: state.xucSacReducer.luaChon,
    soBanThang: state.xucSacReducer.soBanThang,
    soLuotChoi: state.xucSacReducer.soLuotChoi,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handlePlay: () => {
      dispatch(playGameAction());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(KetQua);
