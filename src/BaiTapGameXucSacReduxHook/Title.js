import React from "react";

// export default function Title(props) {
//   return <p className="display-4 text-primary">{props.content}</p>;
// }
export default function Title({ content }) {
  return <p className="display-4 text-primary">{content}</p>;
}
