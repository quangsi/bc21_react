import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { luaChonAction } from "./redux/actions/xucSacAction";
import Title from "./Title";
import { TAI, XIU } from "./xucSacContant";
let btnStyle = {
  padding: "80px",
  fontSize: "40px",
  borderRadius: "16px",
  border: "none",
};
export default function GameXucSacHook() {
  // let mangXucSac = useSelector((state) => {
  //   return state.xucSacReducer.mangXucSac;
  // });
  // let mangXucSac = useSelector((state) => state.xucSacReducer.mangXucSac);

  //  tương ứng với mapStateToProps, có thể dùng nhiều userSelector trong 1 component
  let { mangXucSac } = useSelector((state) => state.xucSacReducer);
  //  tương ứng với mapDispatchToProps
  let dispatch = useDispatch();
  console.log("mangXucSac", mangXucSac);
  return (
    <div className="container">
      {/* <h2>Bài tập game xúc sắc</h2> */}
      <Title content="Bài tập game xúc sắc" />
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          marginTop: "100px",
        }}
      >
        <button
          onClick={() => {
            // this.props.handleLuaChon(TAI);
            dispatch(luaChonAction(TAI));
          }}
          className="btn-danger"
          style={btnStyle}
        >
          Tài
        </button>
        <div>
          {mangXucSac?.map((item) => {
            return (
              <img
                style={{ width: "80px", height: "80px" }}
                src={item.img}
                alt=""
              />
            );
          })}
        </div>
        <button
          onClick={() => {
            // this.props.handleLuaChon(XIU);
            dispatch(luaChonAction(XIU));
          }}
          className="btn-dark"
          style={btnStyle}
        >
          Xỉu
        </button>
      </div>
    </div>
  );
}
