import { combineReducers } from "redux";
import { xucSacReducer } from "./xucSacReducer";

export let rootReducerXucSacHook = combineReducers({
  xucSacReducer,
});
