import { TAI, XIU } from "../../xucSacContant";
import { randomIntFromInterval } from "../../xucSacUtil";
import { LUA_CHON, PLAY_GAME } from "../constants/xucSacContant";

let initialState = {
  mangXucSac: [
    {
      giaTri: 3,
      img: "./img/imgXucSac/3.png",
    },
    {
      giaTri: 3,
      img: "./img/imgXucSac/3.png",
    },
    {
      giaTri: 3,
      img: "./img/imgXucSac/3.png",
    },
  ],
  luaChon: null,
  soBanThang: 2,
  soLuotChoi: 10,
};

export const xucSacReducer = (state = initialState, action) => {
  switch (action.type) {
    case LUA_CHON: {
      //   console.log(action.payload);

      state.luaChon = action.payload;
      return { ...state };
    }

    case PLAY_GAME: {
      let tongDiemXucXacRandom = 0;
      let newMangXucSac = state.mangXucSac.map((item) => {
        let randomNum = randomIntFromInterval(1, 6);
        tongDiemXucXacRandom += randomNum;
        return {
          giaTri: randomNum,
          img: `./img/imgXucSac/${randomNum}.png`,
        };
      });
      let ketQua = tongDiemXucXacRandom >= 11 ? TAI : XIU;
      let newSoLuotChoi = state.soLuotChoi + 1;
      let newSoBanThang =
        state.luaChon === ketQua ? state.soBanThang + 1 : state.soBanThang;
      state.mangXucSac = newMangXucSac;
      state.soLuotChoi = newSoLuotChoi;
      state.soBanThang = newSoBanThang;
      return { ...state };
    }
    default:
      return state;
  }
};
