export const animalsArr = [
  {
    name: "Turkish Angora",
    price: "361.00",
    img: "http://loremflickr.com/640/480/animals",
    id: "1",
  },
  {
    name: "Japanese Bobtail",
    price: "405.00",
    img: "http://loremflickr.com/640/480/animals",
    id: "2",
  },
  {
    name: "Birman",
    price: "204.00",
    img: "http://loremflickr.com/640/480/animals",
    id: "3",
  },
  {
    name: "Burmese",
    price: "15.00",
    img: "http://loremflickr.com/640/480/animals",
    id: "4",
  },
  {
    name: "Highlander",
    price: "409.00",
    img: "http://loremflickr.com/640/480/animals",
    id: "5",
  },
  {
    name: "Munchkin",
    price: "535.00",
    img: "http://loremflickr.com/640/480/animals",
    id: "6",
  },
  {
    name: "Snowshoe",
    price: "158.00",
    img: "http://loremflickr.com/640/480/animals",
    id: "7",
  },
  {
    name: "LaPerm",
    price: "749.00",
    img: "http://loremflickr.com/640/480/animals",
    id: "8",
  },
  {
    name: "Scottish Fold",
    price: "784.00",
    img: "http://loremflickr.com/640/480/animals",
    id: "9",
  },
];
