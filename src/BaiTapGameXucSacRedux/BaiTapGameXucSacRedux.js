import React, { Component } from "react";
// tongDiemXucXacRandom >= 11 ? TAI : XIU;

import bg_game from "../assets/GameXucXac/bgGame.png";
import "./game.css";
import GameXucSac from "./GameXucSac";
import KetQua from "./KetQua";
export default class BaiTapGameXucSacRedux extends Component {
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${bg_game})`,
          height: "100vh",
          width: "100vw",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
        className="bg_game"
      >
        <GameXucSac />
        <KetQua />
      </div>
    );
  }
}
