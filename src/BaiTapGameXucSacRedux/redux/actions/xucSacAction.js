import { LUA_CHON, PLAY_GAME } from "../constants/xucSacContant";

export let luaChonAction = (luaChon) => {
  return {
    type: LUA_CHON,
    payload: luaChon,
  };
};

export let playGameAction = () => {
  return {
    type: PLAY_GAME,
  };
};
