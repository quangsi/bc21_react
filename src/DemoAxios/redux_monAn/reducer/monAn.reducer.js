import { act } from "react-dom/test-utils";
import { cloneDeep } from "../../utils";
import {
  CAP_NHAT_DANH_SACH,
  CHINH_SUA_MON_AN,
} from "../constant/monAn.constant";

let initialState = {
  danhSachMonAn: [],

  foodEdit: null,
};

export const monAnReducer = (state = initialState, action) => {
  switch (action.type) {
    case CAP_NHAT_DANH_SACH: {
      console.log("CAP_NHAT_DANH_SACH", action.payload);
      state.danhSachMonAn = action.payload;
      return { ...state };
    }
    case CHINH_SUA_MON_AN: {
      state.foodEdit = cloneDeep(action.payload);

      return { ...state };
    }
    default:
      return state;
  }
};
