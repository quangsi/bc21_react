import React, { Component } from "react";
import { connect } from "react-redux";
import ItemMonAn from "./ItemMonAn";

class DanhSachMonAn extends Component {
  render() {
    console.log(this.props.danhSach);
    return (
      <div className="container">
        <table className="table">
          <thead>
            <th>ID</th>
            <th>Tên</th>
            <th>Giá</th>
            <th>Hình ảnh</th>
            <th>Mô tả</th>
            <th >Thao tác</th>
          </thead>

          <tbody>
            {this.props.danhSach?.map((monAn) => {
              return <ItemMonAn monAn={monAn} />;
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    danhSach: state.danhSachMonAn,
  };
};
export default connect(mapStateToProps)(DanhSachMonAn);
