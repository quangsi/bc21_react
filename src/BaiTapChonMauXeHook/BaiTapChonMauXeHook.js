import React, { useState } from "react";

export default function BaiTapChonMauXeHook() {
  //   console.log("BaiTapChonMauXeHook");

  //   let [number, setNumber] = useState(10);
  //   let [like, setLike] = useState(10);

  let [imgSrc, setImgSrc] = useState("./img/imgBlackCar.jpg");
  //   let handlePlus = () => {
  //     setNumber(number + 1);
  //   };

  let handleChangeColorCar = (value) => {
    setImgSrc(value);
  };
  return (
    <div>
      BaiTapChonMauXeHook
      {/* <h2> {number}</h2>
      <button onClick={handlePlus} className="btn btn-warning">
        Plus number
      </button> */}
      <div className="container py-5 ">
        <div className="row">
          <img src={imgSrc} alt="" className="col-5" />
          <div className="col-7">
            <button
              onClick={() => {
                handleChangeColorCar("./img/imgRedCar.jpg");
              }}
              className="btn mx-1 btn-danger"
            >
              Red car
            </button>
            <button
              onClick={() => {
                handleChangeColorCar("./img/imgBlackCar.jpg");
              }}
              className="btn mx-1 btn-dark"
            >
              Black car
            </button>
            <button
              onClick={() => {
                handleChangeColorCar("./img/imgSilverCar.jpg");
              }}
              className="btn mx-1 btn-secondary"
            >
              Silver car
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
