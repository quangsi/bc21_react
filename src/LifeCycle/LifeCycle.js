import React, { Component } from "react";
import Footer from "./Footer";
import Header from "./Header";

// mount : sinh ra
// update : cập nhật
// unmount: mất đi

// life cycle sẽ tự động được chạy , có gọi nó ra dùng hay ko thì nó vẫn được chạy ngầm
export default class LifeCycle extends Component {
  state = {
    like: 1,
    share: 1,
  };
  componentDidMount() {
    //   không phải chạy đầu tiên, sẽ được chạy 1 lần duy nhất sau khi giao diện được tạo ( view init )
    console.log("Did mount");
    this.setState({ like: 100 });

    //  thường dùng để gọi api
  }
  componentDidUpdate() {
    //   update
    // tự động được chạy sau khi hàm render chạy
    console.log("did update");
    // trong phase render ko setState
    // this.setState({ like: 100 });
  }
  render() {
    //   tự động chạy khi state thay đổi
    console.log("render");
    return (
      <div className="container py-5">
        {/* {this.state.like < 10 ? <Header />:<></>} */}
        {/* {this.state.like < 10 && <Header />} */}
        <Header />
        <span className="display-4">{this.state.like}</span>{" "}
        <button
          className="btn btn-warning"
          onClick={() => {
            this.setState({ like: this.state.like + 1 });
          }}
        >
          Plus like
        </button>
        <span className="display-4">{this.state.share}</span>{" "}
        <button
          className="btn btn-secondary"
          onClick={() => {
            this.setState({ share: this.state.share + 1 });
          }}
        >
          Plus share
        </button>
        <Footer like={this.state.like} />
      </div>
    );
  }

  componentWillUnmount() {}
}
