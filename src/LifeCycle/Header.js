import React, { Component } from "react";

export default class Header extends Component {
  componentDidMount() {
    this.countdown = setInterval(() => {
      //   console.log("setInterval_header");
    }, 1000);
  }
  render() {
    console.log("render header");
    return <div className="bg-primary p-5">Header</div>;
  }

  componentWillUnmount() {
    //   unmout : mất đi ( die )
    // tự động chạy khi component bị xoá khỏi layout
    // console.log("Header biến mất");
    //
    clearInterval(this.countdown);
  }
}
