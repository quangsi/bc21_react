import "./App.css";
import BaiTapChoMauXe from "./BaiTapChoMauXe/BaiTapChoMauXe";
import BaiTapGameXucSac from "./BaiTapGameXucSac/BaiTapGameXucSac";
import BaiTapGameXucSacRedux from "./BaiTapGameXucSacRedux/BaiTapGameXucSacRedux";
import BaiTapGioHang from "./BaiTapGioHang/BaiTapGioHang";
import BaiTapGioHangRedux from "./BaiTapGioHangRedux/BaiTapGioHangRedux";
import DemoAxios from "./DemoAxios/DemoAxios";
import DemoForm from "./DemoForm/DemoForm";
import DemoMiniRedux from "./DemoMiniRedux/DemoMiniRedux";
import DemoPropsParent from "./DemoProps/DemoPropsParent";
import DemoState from "./DemoState/DemoState";
import LifeCycle from "./LifeCycle/LifeCycle";
import RenderListItem from "./RenderListItem/RenderListItem";
import "antd/dist/antd.css";
import BaiTapChonMauXeHook from "./BaiTapChonMauXeHook/BaiTapChonMauXeHook";
import BaiTapGameXucSacReduxHook from "./BaiTapGameXucSacReduxHook/BaiTapGameXucSacReduxHook";
function App() {
  return (
    <div className="App">
      {/* <DemoState /> */}
      {/* <BaiTapChoMauXe /> */}
      {/* <RenderListItem /> */}
      {/* <DemoPropsParent /> */}

      {/* <BaiTapGioHang /> */}
      {/* <BaiTapGameXucSac /> */}

      {/* <DemoMiniRedux /> */}

      {/* <BaiTapGioHangRedux /> */}
      {/* <BaiTapGameXucSacRedux /> */}
      <BaiTapGameXucSacReduxHook />
      {/* <DemoForm /> */}

      {/* <LifeCycle /> */}
      {/* <DemoAxios /> */}

      {/* <BaiTapChonMauXeHook /> */}
    </div>
  );
}

export default App;
