import React, { Component } from "react";

export default class KetQua extends Component {
  render() {
    let { luaChon, soBanThang, soLuotChoi } = this.props;
    return (
      <div>
        {/* {luaChon && <h4> Kết quả </h4>} */}
        {luaChon && <h3 className=" text-success">Lựa chọn :{luaChon}</h3>}
        {/* {luaChon || <h1>Bạn chưa chọn</h1>} */}
        <h3 className=" text-danger">Số bàn thắng :{soBanThang}</h3>
        <h3 className=" text-dark">Số lượt chơi :{soLuotChoi}</h3>
        <button
          onClick={() => {
            this.props.handlePlay();
          }}
        >
          Play Game
        </button>
      </div>
    );
  }
}
