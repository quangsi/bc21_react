import React, { Component } from "react";
// tongDiemXucXacRandom >= 11 ? TAI : XIU;

import bg_game from "../assets/GameXucXac/bgGame.png";
import "./game.css";
import GameXucSac from "./GameXucSac";
import KetQua from "./KetQua";
import { TAI, XIU } from "./xucSacContant";
import { randomIntFromInterval } from "./xucSacUtil";
export default class BaiTapGameXucSac extends Component {
  state = {
    mangXucSac: [
      {
        giaTri: 3,
        img: "./img/imgXucSac/3.png",
      },
      {
        giaTri: 3,
        img: "./img/imgXucSac/3.png",
      },
      {
        giaTri: 3,
        img: "./img/imgXucSac/3.png",
      },
    ],
    luaChon: null,
    soBanThang: 0,
    soLuotChoi: 0,
  };
  handleLuaChon = (value) => {
    this.setState({ luaChon: value });
  };
  handlePlay = () => {
    let tongDiemXucXacRandom = 0;
    let newMangXucSac = this.state.mangXucSac.map((item) => {
      let randomNum = randomIntFromInterval(1, 6);
      tongDiemXucXacRandom += randomNum;
      console.log(randomNum);
      return {
        giaTri: randomNum,
        img: `./img/imgXucSac/${randomNum}.png`,
      };
    });
    let ketQua = tongDiemXucXacRandom >= 11 ? TAI : XIU;
    let newSoLuotChoi = this.state.soLuotChoi + 1;
    let newSoBanThang =
      this.state.luaChon === ketQua
        ? this.state.soBanThang + 1
        : this.state.soBanThang;
    this.setState({
      mangXucSac: newMangXucSac,
      soLuotChoi: newSoLuotChoi,
      soBanThang: newSoBanThang,
    });
  };
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${bg_game})`,
          height: "100vh",
          width: "100vw",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
        className="bg_game"
      >
        <GameXucSac
          handleLuaChon={this.handleLuaChon}
          mangXucSac={this.state.mangXucSac}
        />
        <KetQua
          soBanThang={this.state.soBanThang}
          soLuotChoi={this.state.soLuotChoi}
          luaChon={this.state.luaChon}
          handlePlay={this.handlePlay}
        />
      </div>
    );
  }
}
