import React, { Component } from "react";
import { TAI, XIU } from "./xucSacContant";

let btnStyle = {
  padding: "80px",
  fontSize: "40px",
  borderRadius: "16px",
  border: "none",
};

export default class GameXucSac extends Component {
  render() {
    return (
      <div className="container">
        <h2>Bài tập game xúc sắc</h2>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            marginTop: "100px",
          }}
        >
          <button
            onClick={() => {
              this.props.handleLuaChon(TAI);
            }}
            className="btn-danger"
            style={btnStyle}
          >
            Tài
          </button>
          <div>
            {this.props.mangXucSac.map((item) => {
              return (
                <img
                  style={{ width: "80px", height: "80px" }}
                  src={item.img}
                  alt=""
                />
              );
            })}
          </div>
          <button
            onClick={() => {
              this.props.handleLuaChon(XIU);
            }}
            className="btn-dark"
            style={btnStyle}
          >
            Xỉu
          </button>
        </div>
      </div>
    );
  }
}
